# CDM-Sci

## What is CDM-Sci?

CDM-Sci stands for "Common Data Model for Scientific Simulation Workflows". As a data model, CDM-Sci is a tool that organizes elements of data and standardizes how they relate to one another and to the properties of real-world entities. As applied to scientific simulations, CDM-Sci is specifically designed to organize elements of data corresponding to the physical and numerical information required to set-up scientific simulations. CDM-Sci has been developed to address multi-physics and multi-scale problems (e.g. multiphase flows involving fluid mechanics, surface sciences, material sciences and/or chemistry). CDM-Sci allows naturally such multi-scale models to be mapped into computational workflows, which link/chain scientific softwares operating at different scales. These computational workflows can then be executed on a set of computational resources following an automatic procedure. 

In short, CDM-Sci provides a common standardized structure (Common Data Model) for the set-up and execution of scientific simulation workflows.


## Why use CDM-Sci?

A first purpose of CDM-Sci is to overcome a current obstacle encountered in multi-scale and multi-physics modeling: the lack of a common ground between scientists working in different disciplines (especially regarding terminology, nomenclature and methodology). This impedes proper communication required to handle such multiscale models involving atomistic (e.g. molecular dynamics), mesoscopic (e.g., coarse-grained molecular dynamics, dissipative particle dynamics) as well as continuum models (e.g., computational fluid dynamics, population balances, discrete element method). CDM-Sci offers a common language to facilitate such necessary interactions across scientific disciplines and scales.

A second purpose of CDM-Sci is to address another issue encountered in multi-scale and multi-physics modeling: the lack of standardized communications between softwares acting at different scales. CDM-Sci offers a user-friendly framework that optimizes the general computational techniques needed to generate code for drivers and to facilitate software coupling. This guarantees that software inter-operability is naturally ensured.


## How does CDM-Sci work?

CDM-Sci provides a unique framework to facilitate coupling/chaining of physical models, which operate at different scales or at different levels of description ranging from molecular dynamics to fluid mechanics, to build multi-physics and multi-scale computational workflows that can be automatically executed. Hence, CDM-Sci is composed of two key ingredients:

* A common data model for scientific computing:

  CDM-Sci offers a solution that collects information in a standardized method in the form a query tree designed to identify the physical and numerical models needed in a given situation. This means that each situation to be simulated is first decomposed into a set of physical components, which are defined as either discrete or continuum systems (quantum systems are not addressed in the present version). Then, a physical description is detailed for each component to fully characterize its nature as well as the physical processes at play. This can lead to introducing a number of sub-components which play a specific physical role. Once the physics at stake (variables, laws, evolution equations) is clarified, a numerical description is chosen either in terms of a set of interacting particles or continuous fields for every component or sub-component. By resorting to this combined physical and numerical points of view, the proposed solution offers naturally a way to specify the various numerical softwares needed to simulate the systems of interest. In addition, regardless of whether the workflow requires a single software or to couple several ones, the information exchanged between components, or sub-components, is clearly identified thanks to specific queries. Finally, when the workflow combines several softwares, similar queries allow to identify the information exchanged between them (inputs/outputs identification), as well as the transformations required (mappers) and the format of the data to be exchanged (wrappers).
  Another specificity of CDM-Sci is that it contains consistency checks used to verify that the choices made in the various physical models are compatible with each other (in terms of the physics involved, or in terms of the levels of description & information content chosen).
  
* Computational sciences tools for automatic code generation and execution:

  In order to facilitate coding of new models and execution of calculation workflows coupling different models (like discrete and continuum models), new tools for automatic code generation are developed and validated. For that purpose, CDM-Sci relies on a Model Driven Architecture (MDA) development approach, which is based on the assumption that codes can be produced from abstract, human-elaborated modelling diagrams. It allows to generate standardized data to describe models, such as XSD (XML Schema Definition). 
  

## Who is concerned by CDM-Sci?

CDM-Sci is of great interest for physics-oriented users as well as computer scientists.

* From the perspective of physics-oriented users, CDM-Sci allows engineers and scientists to go beyond the simulation set-ups they are familiar with by offering them the possibility to explore new multi-scale physics by linking/coupling with models developed in nearby areas.
* From a computer science perspective, CDM-Sci can help experts to understand the needs of engineers/scientists, so that they can adapt existing computational techniques or orient new developments to make them accessible and relevant to engineers/scientists.

In short, CDM-Sci bridges the gap between physics-oriented users and computer scientists.

## How is the Gitlab organized?
CDM-Sci contains three folders:

* Documentation:

  This folder contains a detailed description of the CDM.

* Software:

  This folder contains the python script, which allows to declare all the data related to a scientific workflow and to export it (e.g. in an XSD file as the one available in the folder).

* Tutorials & Examples: (coming soon)

  This folder contains a few examples (to illustrate how the CDM applies to some selected workflows) as well as trainings for new users.

## Support
Users with questions regarding the CDM are invited to contact Christophe Henry and Jean-Pierre Minier.

## Contributing
CDM-Sci is still under development. 
People willing to contribute to the project are more than welcome and are invited to contact the authors. We are especially looking for people who want to test the CDM in new practical applications and to enrich the CDM with aspects that are currently overlooked or only briefly addressed (e.g. magneto-hydrodynamics, solid mechanics, active matter).

## Authors and acknowledgment
CDM-Sci is the result of a collaboration between 5 authors (in alphabetic order): [Mireille Bossy](http://www-sop.inria.fr/members/Mireille.Bossy/) (Inria), [Eric Fayolle](https://www.researchgate.net/profile/Eric-Fayolle) (EDF), [Christophe Henry](http://www-sop.inria.fr/members/Christophe.Henry/) (Inria), [Jean-Pierre Minier](https://www.researchgate.net/profile/Jean-Pierre-Minier) (EDF) and Pascale Noyret (EDF).
CDM-Sci was initiated within the framework of the [VIMMP project](https://www.vimmp.eu/) (funded from the European Union’s Horizon 2020 research and innovation program under grant agreement No [760907](https://cordis.europa.eu/project/id/760907)).

The authors are grateful to a number of colleagues for constructive and insightful discussions, with a special mention to Mara Chiricotto (STFC, UK), Martin Ferrand (EDF, FR), Florence Marcotte (Inria, FR), Yannick Ponty (OCA, FR), Robert Sawko (IBM, UK).

## License
CDM-Sci is a free open-source project available under LGPL License.

